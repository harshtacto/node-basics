var express = require('express');
var bodyParser = require('body-parser');
var UserController = require('../controllers/user');

var router = express.Router();

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

router.get('/getAll', UserController.getAll);
router.post('/getUser/:id', UserController.getUser);

module.exports = router;