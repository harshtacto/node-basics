var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var UserModel = new Schema({
  name: { type: String, required: true },
  password: { type: String, required: true },
  dob: Date,
  age: Number,
  isMarried: { type: Boolean, default: false },
  education: {
    scc: Number,
    hsc: Number,
    graduation: Number,
  }
});

module.exports = mongoose.model('user', UserModel);