var express = require('express');
var bodyParser = require('body-parser');
var UserRoutes = require('./routes/user');

var port = 3000;
var app = express();

app.use('/api/v1/user', UserRoutes);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/', function (req, res) {
  res.send('Hello World');
});

app.post('/login', function (req, res) {
  var request = req.body;

  if (request.username && request.username === 'harsh') {
    if (request.password && request.password === '123') {
      res.send('Successfully Login');
    } else {
      res.send('Password is incorrect');
    }
  } else {
    res.send('Username not found');
  }
});

app.listen(port, function (err) {
  if (err) console.log(err);
  else console.log('Server is running on port: 3000');
});

